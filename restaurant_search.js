/**
 * name:        Restaurant_search.js
 * description: A Restaurant search JS script
 * input:       .csv file containing the restaurant data
 * output:      '\n' delimited String
 */


 /**
    Get the 'restaurant.csv file and user date&time input' through 'Hotelsearch.html' file .Read the restaurant.csv file  
    and Extract the data from csv file and split the data on char '/'.
 */
function File_upload() {
    var input_data=document.getElementById("input_data").value;
	var fileUpload = document.getElementById("upload");
	var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
       
    if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                reader.onload = function (e) {
                const lines= reader.result.split('\n').map(function (line){
                	return line.replace(',','/').split("/")
                })
                restaurant_search(lines,input_data);
            }
                reader.readAsText(fileUpload.files[0]);
        } else {
                alert("This browser does not support HTML5.");
            }
    } else {
            alert("Please upload a valid CSV file.");
    }
}

/**
    i/p: restaurant information, user date information, user_time ,user_time_format
    o/p: days and time is match return 1;

    divide the csv file data into 3 step and based on the format process on data if restaurant is open return 1.
*/
function Main_check(lines_data,process_date,input_data,time_format) {
    const days_array=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
    const days_array_2_3=["Mon","Tue","Wed","Thu","Fri","Sat","Sun"];
    var flag=0;
        if(lines_data.match(/[a-z]{3}-[a-z]{3}, [a-z]{3}/i)) {//3

                if(lines_data.match(/[a-z]{3}-[a-z]{3}/i)) {
                        var x=0;
                        var flag_not_found=0;
                        var lines_array=lines_data.split(' ');
                        var data=lines_data.match((/[a-z]{3}-[a-z]{3}/i));
                        var days=data[0].split('-');
                        var day=days_array[process_date];
                        for(var i=days_array_2_3.indexOf(days[0]);i<=days_array_2_3.indexOf(days[1]);i++) {
                            if(days_array_2_3[i]==day) {
                                var x=Time_handler(input_data.split(':'),time_format,lines_array[2].split(':'),lines_array[3],lines_array[5].split(':'),lines_array[6].slice(0,2));
                                if(x==1) {
                                    flag_not_found=1;         
                                }
                            }
                        }
                        if(flag_not_found==0) {
                            var data=lines_data.match((/ [a-z]{3}/i));
                            var day1=" "+days_array_2_3[process_date];
                                if(data==day1) {
                                    var x=Time_handler(input_data.split(':'),time_format,lines_array[2].split(':'),lines_array[3],lines_array[5].split(':'),lines_array[6].slice(0,2));
                                    if(x==1) {
                                        flag_not_found=1;
                                    }
                                }
                        }
                        if(flag_not_found==1)
                            return 1;
                        flag=1;
            } 
        } 
        if(flag==0) {//2 
            if(lines_data.match(/[a-z]{3}-[a-z]{3}/i)) {
                    var x=0;
                    var lines_array=lines_data.split(' ');
                    var data=lines_data.match((/[a-z]{3}-[a-z]{3}/i));
                    var days=data[0].split('-');
                    var day=days_array[process_date];
                    for(var i=days_array_2_3.indexOf(days[0]);i<=days_array_2_3.indexOf(days[1]);i++) {
                        
                        if(days_array_2_3[i]==day) {

                                if(lines_array[0]=="") {
                                    var x=Time_handler(input_data.split(':'),time_format,lines_array[2].split(':'),lines_array[3],lines_array[5].split(':'),lines_array[6].slice(0,2));
                                }
                                else {
                                    var x=Time_handler(input_data.split(':'),time_format,lines_array[1].split(':'),lines_array[2],lines_array[4].split(':'),lines_array[5].slice(0,2));
                                }
                                if(x==1)
                                    return 1;     
                            }
                    }
            }
        }
        
        if(lines_data.match(/ [a-z]{3} /i)) {//1
            var lines_array=lines_data.split(' ');
            var day=days_array[process_date];
            if(lines_array[1]==day) {
                var x=Time_handler(input_data.split(':'),time_format,lines_array[2].split(':'),lines_array[3],lines_array[5].split(':'),lines_array[6].slice(0,2));
                if(x==1)
                return 1;
            }
        }
    }

/**
    i/p: user_date,time format,(am/pm), restaurant openning_time,closing_time, and there time format
    o/p: return 1 if restaurant is open.
    check the time weather resturant is open or not

*/
function Time_handler(input_data ,time_format,start_time,AM,end_time,PM) {
    if(AM=="am") {
    start_time=parseInt(start_time[0])+12;
        if(start_time>24)
            start_time=0;
    }
    if(PM=="am") {
        end_time=parseInt(end_time[0])+12;
        if(end_time>24)
            end_time=0;
    }
    if(AM=="pm") {
        start_time=parseInt(start_time[0])+12;
    }
    if(PM=="pm") {
        end_time=parseInt(end_time[0])+12;
    }
    if(time_format=="am") {
        update_time=parseInt(input_data[0])+12;
        if(update_time==24) {
            update_time=0;
        }
    }else { 
        update_time=input_data[0];
    }
    if(time_format=="am"&&update_time==0) {
            for(let i=start_time;i!=end_time;i=(i+1)%24){
            if(i== update_user_time[0]){
                return 1;
            }
    }else {
        if(start_time<=update_time&&end_time>=update_time){
            return 1;
        }
    }

 }

/**
    i/p: restaurant inforamation , user time information.
    o/p: Print the open restaurant_information.
    get the csv file data and send for process on data.
*/
function restaurant_search(line,input_data) {
        var count=0;
        var date=process_on_user_datetime(input_data);
        var time=input_data.split(' ');
        for(var i=0;i<line.length;i++) {
            for(var j=1;j<line[i].length;j++) {
                var x=Main_check(line[i][j],date,time[3],time[4]);
                if(x==1) {
                    console.log(line[i][0]);
                    count=count+1;
                }
            }
        }
        console.log(count);
}

/**
    i/p: user_date & time  information.
    o/p: return day information.
    Takes user date and time input and convert it into day
*/
function process_on_user_datetime(input_data) {
    var A=new Date(input_data);
    var B=A.getDay();
    return B;
}
